# Ergo Vox

A light and fast screen reader that can be used on PCs or embedded systems,
allowing for greater accessibility.

## Introduction 

Ergo Vox is a screen reader service, that is meant to be lightweight, fast,
and unobtrusive. It uses _libatspi_ and _libspeechd_ to interface with the
accessibility interface and synthesize audio.

## Donate

Please consider donating to help cover the costs of developing and maintaining
the project. You can send donations with the following methods. To donate using
other cryptocurrencies, use the address from the _Monero_ link below, as the
_receive_ address on an exchange such as [ChangeNOW](https://changenow.io/) or
similar. Thank you.

* [Monero](monero:87apvQxCW7LgCPjz7muRBedPf19TexjFAT3gjdyVKk6bVdD5cr7NGohbYCC6JKtKeM2a93tiWmTzj7hho4fRaZav5rJd7Y1)
* [LiberaPay](https://liberapay.com/dobey)
* [CashApp](https://cash.app/$dohbee)
* [Patreon](https://patreon.com/dobey)

## License

This project is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as
published by the Free Software Foundation.

This project is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
