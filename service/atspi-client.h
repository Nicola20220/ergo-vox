/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "client.h"

#include <core/signal.h>

#include <memory>

namespace ergo {
namespace vox {

    class Atspi : public Client
    {
    public:
        Atspi();
        virtual ~Atspi();

        core::Signal<const Event&>& event() override;

        virtual bool register_event(const std::string& type) override;

    private:
        class Impl;
        std::unique_ptr<Impl> p;

        // disable copying
        Atspi(const Atspi&) = delete;
        Atspi& operator=(const Atspi&) = delete;
    };

} // vox
} // ergo
