/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

namespace ergo {
namespace vox {

    struct Event {
        // 
        typedef enum {
            INVALID = 0,
            ACCELERATOR_LABEL,
            ALERT,
            ANIMATION,
            ARROW,
            CALENDAR,
            CANVAS,
            CHECK_BOX,
            CHECK_MENU_ITEM,
            COLOR_CHOOSER,
            COLUMN_HEADER,
            COMBO_BOX,
            DATE_EDITOR,
            DESKTOP_ICON,
            DESKTOP_FRAME,
            DIAL,
            DIALOG,
            DIRECTORY_PANE,
            DRAWING_AREA,
            FILE_CHOOSER,
            FILLER,
            FOCUS_TRAVERSABLE,
            FONT_CHOOSER,
            FRAME,
            GLASS_PANE,
            HTML_CONTAINER,
            ICON,
            IMAGE,
            INTERNAL_FRAME,
            LABEL,
            LAYERED_PANE,
            LIST,
            LIST_ITEM,
            MENU,
            MENU_BAR,
            MENU_ITEM,
            OPTION_PANE,
            PAGE_TAB,
            PAGE_TAB_LIST,
            PANEL,
            PASSWORD_TEXT,
            POPUP_MENU,
            PROGRESS_BAR,
            PUSH_BUTTON,
            RADIO_BUTTON,
            RADIO_MENU_ITEM,
            ROOT_PANE,
            ROW_HEADER,
            SCROLL_BAR,
            SCROLL_PANE,
            SEPARATOR,
            SLIDER,
            SPIN_BUTTON,
            SPLIT_PANE,
            STATUS_BAR,
            TABLE,
            TABLE_CELL,
            TABLE_COLUMN_HEADER,
            TABLE_ROW_HEADER,
            TEAROFF_MENU_ITEM,
            TERMINAL,
            TEXT,
            TOGGLE_BUTTON,
            TOOL_BAR,
            TOOL_TIP,
            TREE,
            TREE_TABLE,
            UNKNOWN,
            VIEWPORT,
            WINDOW,
            EXTENDED,
            HEADER,
            FOOTER,
            PARAGRAPH,
            RULER,
            APPLICATION,
            AUTOCOMPLETE,
            EDITBAR,
            EMBEDDED,
            ENTRY,
            CHART,
            CAPTION,
            DOCUMENT_FRAME,
            HEADING,
            PAGE,
            SECTION,
            REDUNDANT_OBJECT,
            FORM,
            LINK,
            INPUT_METHOD_WINDOW,
            TABLE_ROW,
            TREE_ITEM,
            DOCUMENT_SPREADSHEET,
            DOCUMENT_PRESENTATION,
            DOCUMENT_TEXT,
            DOCUMENT_WEB,
            DOCUMENT_EMAIL,
            COMMENT,
            LIST_BOX,
            GROUPING,
            IMAGE_MAP,
            NOTIFICATION,
            INFO_BAR,
            LEVEL_BAR,
            TITLE_BAR,
            BLOCK_QUOTE,
            AUDIO,
            VIDEO,
            DEFINITION,
            ARTICLE,
            LANDMARK,
            LOG,
            MARQUEE,
            MATH,
            RATING,
            TIMER,
            STATIC,
            MATH_FRACTION,
            MATH_ROOT,
            SUBSCRIPT,
            SUPERSCRIPT,
            DESCRIPTION_LIST,
            DESCRIPTION_TERM,
            DESCRIPTION_VALUE,
            FOOTNOTE
        } Role;
        Role role = INVALID;

        std::string type;
        std::string title;
    };

} // vox
} // ergo
