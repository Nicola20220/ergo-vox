/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atspi-client.h"
#include "engine.h"
#include "speechd-synth.h"

#include <glib/gi18n.h>
#include <gio/gio.h>

using namespace ergo::vox;

// LCOV_EXCL_START
int main(int /* argc */, char** /* argv */)
{
    setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_PACKAGE, GETTEXT_LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    auto loop = g_main_loop_new(nullptr, false);

    // Set up the AT-SPI handling
    auto client = std::make_shared<Atspi>();
    auto synth = std::make_shared<Speechd>();
    auto engine = std::make_shared<Engine>(client, synth);

    synth->speak_string("<voice xml:lang=\"la\">Ergo Vox Machina</voice>");

    // start processing
    g_main_loop_run(loop);

    g_main_loop_unref(loop);
    return 0;
}
// LCOV_EXCL_STOP
