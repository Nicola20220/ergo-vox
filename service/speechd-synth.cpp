/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "speechd-synth.h"

#include <libspeechd.h>
#include <glib.h>

namespace ergo {
namespace vox {

class Speechd::Impl {
public:
    Impl()
    {
        auto address = spd_get_default_address(nullptr);
        if (address != nullptr) {
            gchar* error = nullptr;
            m_spd_conn = spd_open2("ergo-vox", "main", g_get_user_name(),
                                   SPD_MODE_THREADED, address, true, &error);
            if (error != nullptr) {
                g_critical("%s", error);
                g_free(error);
            }
            if (m_spd_conn == nullptr) {
                // LCOV_EXCL_START
                g_critical("Failed to connect to speech-dispatcher service.");
                // LCOV_EXCL_STOP
            } else {
                spd_set_data_mode(m_spd_conn, SPD_DATA_SSML);
            }
            SPDConnectionAddress__free(address);
        }
    }

    ~Impl()
    {
        if (m_spd_conn != nullptr) {
            spd_close(m_spd_conn);
        }
    }

    void speak_string(const std::string& value)
    {
        spd_cancel_all(m_spd_conn);
        spd_say(m_spd_conn, SPD_MESSAGE, value.c_str());
    }

private:
    SPDConnection* m_spd_conn = nullptr;
}; // class Impl


Speechd::Speechd() :
    p(new Impl())
{
}

Speechd::~Speechd()
{
}

void Speechd::speak_string(const std::string& value)
{
    p->speak_string(value);
}

} // vox
} // ergo
