# Copyright 2019 Rodney Dawes
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
import dbus
from dbusmock import mockobject


BUS_NAME = 'org.a11y.Bus'
MAIN_OBJ = '/org/a11y/bus'
MAIN_IFACE = 'org.a11y.Bus'
SYSTEM_BUS = False

A11Y_BUS_IFACE = MAIN_IFACE
A11Y_STATUS_IFACE = 'org.a11y.Status'

ATSPI_BUS_PREFIX = 'org.a11y.atspi'
ATSPI_OBJ_PREFIX = '/org/a11y/atspi'

def load(mock, parameters):
    mock.AddMethods(A11Y_BUS_IFACE, [
        ('GetAddress', '', 's',
         """
ret = dbus.String(os.environ['DBUS_SESSION_BUS_ADDRESS'])
         """
        ),
    ])
    mock.AddProperties(A11Y_STATUS_IFACE, {
        'IsEnabled': True,
        'ScreenReaderEnabled': False,
    })
