# Copyright 2019 Rodney Dawes
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
import dbus
from dbusmock import mockobject

SPI_DBUS_NAME_PREFIX = 'org.a11y.atspi'
SPI_DBUS_PATH_PREFIX = '/org/a11y/atspi'
SPI_DBUS_IFACE_PREFIX = 'org.a11y.atspi'

SPI_DBUS_PATH_NULL = SPI_DBUS_PATH_PREFIX + '/null'
SPI_DBUS_PATH_ROOT = SPI_DBUS_PATH_PREFIX + '/accessible/root'

SPI_DBUS_NAME_REGISTRY = SPI_DBUS_NAME_PREFIX + '.Registry'
SPI_DBUS_IFACE_REGISTRY = SPI_DBUS_IFACE_PREFIX + '.Registry'
SPI_DBUS_PATH_REGISTRY = SPI_DBUS_PATH_PREFIX + '/registry'

SPI_DBUS_IFACE_DEC = SPI_DBUS_IFACE_PREFIX + '.DeviceEventController'
SPI_DBUS_IFACE_DEL = SPI_DBUS_IFACE_PREFIX + '.DeviceEventListener'
SPI_DBUS_PATH_DEC = SPI_DBUS_PATH_PREFIX + '/registry/deviceventcontroller'

SPI_DBUS_IFACE_CACHE = SPI_DBUS_IFACE_PREFIX + '.Cache'
SPI_DBUS_PATH_CACHE = SPI_DBUS_PATH_PREFIX + '/cache'

SPI_DBUS_IFACE_ACCESSIBLE = SPI_DBUS_IFACE_PREFIX + '.Accessible'
SPI_DBUS_IFACE_APPLICATION = SPI_DBUS_IFACE_PREFIX + '.Application'
SPI_DBUS_IFACE_COMPONENT = SPI_DBUS_IFACE_PREFIX + '.Component'
SPI_DBUS_IFACE_EVENT_KB = SPI_DBUS_IFACE_PREFIX + '.Keyboard'
SPI_DBUS_IFACE_EVENT_MOUSE = SPI_DBUS_IFACE_PREFIX + '.Event.Mouse'
SPI_DBUS_IFACE_EVENT_OBJ = SPI_DBUS_IFACE_PREFIX + '.Event.Object'
SPI_DBUS_IFACE_SOCKET = SPI_DBUS_IFACE_PREFIX + '.Socket'

BUS_NAME = SPI_DBUS_NAME_REGISTRY
MAIN_OBJ = SPI_DBUS_PATH_REGISTRY + 'mock'
MAIN_IFACE = SPI_DBUS_IFACE_REGISTRY + 'Mock'
SYSTEM_BUS = False

def load(mock, parameters):
    mock.AddObject(SPI_DBUS_PATH_ROOT, SPI_DBUS_IFACE_ACCESSIBLE,
                   {
                       'childCount': dbus.Int32(0),
                       'description': '',
                       'name': '',
                       'parent': ('', ''),
                   },
                   [
                       ('GetApplication', '', '(so)', ''),
                       ('GetAttributes', '', 'a{ss}', ''),
                       ('GetChildAtIndex', 'i', '(so)', ''),
                       ('GetChildren', '', 'a(so)', ''),
                       ('GetIndexInParent', '', 'i', ''),
                       ('GetLocalizedRoleName', '', 's', ''),
                       ('GetRelationSet', '', 'a(ua(so))', ''),
                       ('GetRole', '', 'u', ''),
                       ('GetRoleName', '', 's', ''),
                       ('GetState', '', 'au', ''),
                   ])
    mock.AddObject(SPI_DBUS_PATH_CACHE, SPI_DBUS_IFACE_CACHE,
                   {
                   },
                   [
                   ]);
    mock.AddObject(SPI_DBUS_PATH_REGISTRY, SPI_DBUS_IFACE_REGISTRY,
                   {
                   },
                   [
                       ('RegisterEvent', 'sas', '',
                        """
self.EmitSignal('org.a11y.atspi.Registry',
    'EventListenerRegistered', 'ss',
    [':1.4', 'Window:Activate'])
self.EmitSignal('org.a11y.atspi.Event.Window',
    'Activate', 'siiv(so)',
    ['', 0, 0, '', (':1.4', '/org/a11y/atspi/accessible/root'),])
                        """),
                       ('DeregisterEvent', 's', '', ''),
                       ('GetRegisteredEvents', '', 'a(ss)', ''),
                   ])
